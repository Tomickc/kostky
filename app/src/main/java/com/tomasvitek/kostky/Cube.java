package com.tomasvitek.kostky;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Tomáš Vítek on 09.05.2016 at 15:15.
 */
public class Cube {

    public int x, y, number, randX, randY, change;
    public float r, randR;
    public boolean goBack,goDownRight,goUpLeft,goUpRight,goDownLeft;

    public void setX(int x) {
        this.x = x;
    }

    public void setGoBack(boolean goBack) {
        this.goBack = goBack;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setChange(int change) {
        this.change = change;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setR(float r) {
        this.r = r;
    }

    public void setGoDownRight(boolean goDownRight) {
        this.goDownRight = goDownRight;
    }

    public void setGoUpLeft(boolean goUpLeft) {
        this.goUpLeft = goUpLeft;
    }

    public void setGoUpRight(boolean goUpRight) {
        this.goUpRight = goUpRight;
    }

    public void setGoDownLeft(boolean goDownLeft) {
        this.goDownLeft = goDownLeft;
    }

    public void onDrawCube(Canvas canvas) {



        //create black color
        Paint grey = new Paint();
        grey.setAntiAlias(true);
        grey.setColor(Color.parseColor("#757575"));
        grey.setStyle(Paint.Style.FILL);

        Paint black = new Paint();
        black.setAntiAlias(true);
        black.setColor(Color.parseColor("#959595"));
        black.setStyle(Paint.Style.FILL);


        //create white color
        Paint white = new Paint();
        white.setAntiAlias(true);
        white.setColor(Color.WHITE);
        white.setStyle(Paint.Style.FILL);


        canvas.save();
        canvas.rotate(r, 125 + x, 125 + y);
        canvas.drawRect(x, y, 250 + x, 250 + y, grey);
        canvas.drawRect(10 + x, 10 + y, 240 + x, 240 + y, black);



            //draw random dots
            switch (number) {
                case 1:
                    canvas.drawCircle(125 + x, 125 + y, 20, white);
                    break;
                case 2:
                    canvas.drawCircle(50 + x, 50 + y, 20, white);
                    canvas.drawCircle(200 + x, 200 + y, 20, white);
                    break;
                case 3:
                    canvas.drawCircle(50 + x, 50 + y, 20, white);
                    canvas.drawCircle(125 + x, 125 + y, 20, white);
                    canvas.drawCircle(200 + x, 200 + y, 20, white);
                    break;
                case 4:
                    canvas.drawCircle(50 + x, 50 + y, 20, white);
                    canvas.drawCircle(50 + x, 200 + y, 20, white);
                    canvas.drawCircle(200 + x, 50 + y, 20, white);
                    canvas.drawCircle(200 + x, 200 + y, 20, white);
                    break;
                case 5:
                    canvas.drawCircle(50 + x, 50 + y, 20, white);
                    canvas.drawCircle(50 + x, 200 + y, 20, white);
                    canvas.drawCircle(125 + x, 125 + y, 20, white);
                    canvas.drawCircle(200 + x, 50 + y, 20, white);
                    canvas.drawCircle(200 + x, 200 + y, 20, white);
                    break;
                case 6:
                    canvas.drawCircle(50 + x, 50 + y, 20, white);
                    canvas.drawCircle(50 + x, 125 + y, 20, white);
                    canvas.drawCircle(50 + x, 200 + y, 20, white);

                    //right
                    canvas.drawCircle(200 + x, 50 + y, 20, white);
                    canvas.drawCircle(200 + x, 125 + y, 20, white);
                    canvas.drawCircle(200 + x, 200 + y, 20, white);
                    break;
            }

        canvas.restore();
    }

}

