package com.tomasvitek.kostky;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

/**
 * Created by Tomáš Vítek on 06.04.2016 at 11:37.
 */

//Custom adapter with my own layout
public class DrawerAdapter extends ArrayAdapter<Player> {

    private final Activity context;
    private final List<Player> players;

    public DrawerAdapter(Activity context, List<Player> players) {
        super(context, R.layout.item_drawer, players);

        this.context = context;
        this.players = players;
    }

    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.item_drawer, null, true);


        TextView name = (TextView)rowView.findViewById(R.id.tvName);
        TextView lastNums = (TextView)rowView.findViewById(R.id.tvLastNumbers);
        TextView diameter = (TextView)rowView.findViewById(R.id.tvDiameter);

        name.setText(players.get(position).name);
        if(players.get(position).throwenNumbers!=null){
            lastNums.setText(players.get(position).throwenNumbers.toString().replace("[", "").replace("]", ""));
        }else  lastNums.setText("");
        if(players.get(position).diameterValue!=0){
            diameter.setText(String.valueOf(players.get(position).diameterValue));
        }else  diameter.setText("");


        return rowView;
    }
}