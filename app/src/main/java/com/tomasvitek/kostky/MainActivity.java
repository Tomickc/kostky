package com.tomasvitek.kostky;


import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.tomasvitek.kostky.fragments.MenuFragment;
import com.tomasvitek.kostky.fragments.PlayersChooseFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //How many cubes user wants display
    public int cubes;

    public ArrayList names;
    public List<Player> players;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ListView listView;
    private ImageView imageView;
    private DrawerAdapter drawerAdapter;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        resetDrawer();
    }

    //Save player
    public void setPlayers( List<Player> players) {
        this.players = players;
    }

    //Save names
    public void setNames(ArrayList names) {
        this.names = names;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Show default fragment
        showFragment(MenuFragment.newInstance(), MenuFragment.TAG, false);

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        listView = (ListView)findViewById(R.id.listDrawer);
        imageView = (ImageView)findViewById(R.id.imageView3);



        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {


                //Checking if the item is in checked state or not, if not make it in checked state
                if(menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();

                return true;
            }
        });

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.openDrawer, R.string.closeDrawer){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);






    }

    private void resetDrawer(){
        listView.setVisibility(View.INVISIBLE);
        imageView.setVisibility(View.VISIBLE);

    }

    public void openDrawer(List<Player> players){
        listView.setVisibility(View.VISIBLE);
        drawerAdapter = new DrawerAdapter(this, players);
        listView.setAdapter(drawerAdapter);

        drawerLayout.openDrawer(navigationView);
        imageView.setVisibility(View.INVISIBLE);

    }

    public void onMenuClick(View view) {

        //Show playground fragment
        showFragment(PlayersChooseFragment.newInstance(), PlayersChooseFragment.TAG, true);

        //Get cubes' value
        switch (String.valueOf(view.getTag())) {
            case "1":

                setCubes(1);
                break;
            case "2":
                setCubes(2);
                break;
            case "3":
                setCubes(3);
                break;
            case "4":
                setCubes(4);
                break;
        }
    }

    public void showFragment(Fragment fragment, String tag, boolean backStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, fragment, tag);
        if (backStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    public void setCubes(int cubes) {
        this.cubes = cubes;
    }

}
