package com.tomasvitek.kostky;

import java.util.ArrayList;

/**
 * Created by Tomáš Vítek on 02.06.2016 at 23:11.
 */

public class Player {

    public String name;
    public ArrayList<Integer> throwenNumbers,estimatedNumber;
    public int diameterValue,allNumbers;


    public void setAllNumbers(int allNumbers) {
        this.allNumbers = allNumbers;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setThrowenNumbers(ArrayList<Integer> throwenNumbers) {
        this.throwenNumbers = throwenNumbers;
    }

    public void setEstimatedNumber(ArrayList<Integer> estimatedNumber) {
        this.estimatedNumber = estimatedNumber;
    }

    public void setDiameterValue(int diameterValue) {
        this.diameterValue = diameterValue;
    }
}
