package com.tomasvitek.kostky.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.tomasvitek.kostky.R;

/**
 * Created by Tomáš Vítek on 09.05.2016 at 14:05.
 */
public class BaseFragment extends Fragment {


    public void showFragment(Fragment fragment, String tag, boolean backStack) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, fragment, tag);
        if (backStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

}
