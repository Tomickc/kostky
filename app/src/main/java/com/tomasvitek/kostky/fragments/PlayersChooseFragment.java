package com.tomasvitek.kostky.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.tomasvitek.kostky.MainActivity;
import com.tomasvitek.kostky.Player;
import com.tomasvitek.kostky.R;

import java.util.ArrayList;

/**
 * Created by Tomáš Vítek on 20.04.2016 at 17:45.
 */
public class PlayersChooseFragment extends BaseFragment {

    //Set fragment's TAG
    public static final String TAG = "players_choose_fragment";

    //Define variables and views
    private Button play;
    private EditText playersCountET;
    private int players;
    private ArrayList<Player> names;
    private ArrayList<String> strings;
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_players_choose, container, false);

        //declare views
        play = (Button) view.findViewById(R.id.button2);
        playersCountET = (EditText) view.findViewById(R.id.editText);
        listView = (ListView) view.findViewById(R.id.listView);

        //When user type into editText than do:
        playersCountET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //Refresh arrayList
                strings = new ArrayList<>();
                names = new ArrayList<>();

                //Check if editText is not empty
                if (playersCountET.getText().toString() != "") {

                    //Treat players count
                    try {
                        players = Integer.parseInt(playersCountET.getText().toString());
                    } catch (Exception e) {
                        players = 0;
                    }

                    //Create new instances of Player and add them in list than add player name into second list
                    for (int i = 1; i <= players; i++) {
                        Player player = new Player();
                        names.add(player);
                        player.setName("Hráč " + String.valueOf(i));
                        strings.add("Hráč " + String.valueOf(i));
                    }

                    //Create new ArrayAdapter with players' names
                    ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, strings);

                    //Set adapter with names
                    listView.setAdapter(arrayAdapter);

                    //If listItem is clicked user can change player's name
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {



                            //Show alertDialog with choice of name change
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            final EditText edittext = new EditText(getActivity());
                            alert.setTitle("Úprava jména");
                            edittext.setHint(names.get(position).name);
                            alert.setView(edittext);

                            alert.setPositiveButton("Potvrdit", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                    //Refresh name in list
                                    String changedName = edittext.getText().toString();
                                    if(changedName.equals("")){
                                        changedName = names.get(position).name;
                                    }
                                    Player player = new Player();
                                    player.setName(changedName);
                                    names.set(position, player);
                                    strings.set(position, changedName);

                                    ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, strings);
                                    listView.setAdapter(arrayAdapter);
                                }
                            });

                            alert.setNegativeButton("Zpět", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //dismiss dialog
                                }
                            });

                            alert.show();


                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Check if playerCount isn't 0 and start game
                if (playersCountET.getText().toString().equals("") || playersCountET.getText().toString().equals("0")) {
                    Toast.makeText(getActivity(), "Nebyl vyplněný počet hráčů", Toast.LENGTH_SHORT).show();
                } else {
                    ((MainActivity) getActivity()).setNames(names);
                    showFragment(PlaygroundFragment.newInstance(), PlaygroundFragment.TAG, true);
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    //Create new instance of this fragment
    public static PlayersChooseFragment newInstance() {
        PlayersChooseFragment fragment = new PlayersChooseFragment();
        return fragment;
    }
}
