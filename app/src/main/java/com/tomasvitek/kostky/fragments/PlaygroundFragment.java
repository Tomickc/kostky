package com.tomasvitek.kostky.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tomasvitek.kostky.MainActivity;
import com.tomasvitek.kostky.R;
import com.tomasvitek.kostky.views.PlayTable;

/**
 * Created by Tomáš Vítek on 20.04.2016 at 17:45.
 */
public class PlaygroundFragment extends BaseFragment {

    //Set fragment's TAG
    public static final String TAG = "playground_fragment";

    //Declare objects
    private View view;
    private int i = 0;
    private String message;
    private PlayTable playTable;
    private ImageView sortBtn;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_playground, null);

        //Declare views
        sortBtn = (ImageView) view.findViewById(R.id.sortBtn);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearlayout);

        //create new instance of playTable
        playTable = new PlayTable(getActivity(), view);

        //add playtable to view
        linearLayout.addView(playTable);

        //set quantity of cubes
        playTable.setCube(((MainActivity) getActivity()).cubes);


        sortBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Show current player's statistics


                for(int i = 0; i < ((MainActivity)getActivity()).players.size(); i++) {
                    if(i==0){
                        if (((MainActivity) getActivity()).players.get(i).throwenNumbers != null) {
                            message = "Hráč: " + (((MainActivity) getActivity()).players.get(i).name) + "\nPoslední hozená čísla:\n" + ((MainActivity) getActivity()).players.get(i).throwenNumbers.toString().replace("[", "").replace("]", "") + "\n" +
                                    "Průměrná hodnota: " + ((MainActivity) getActivity()).players.get(i).diameterValue;
                        }else message="Hráč: " + (((MainActivity) getActivity()).players.get(i).name);

                    }else {
                        if (((MainActivity) getActivity()).players.get(i).throwenNumbers != null) {
                            message += "\n\nHráč: " + (((MainActivity) getActivity()).players.get(i).name) + "\nPoslední hozená čísla:\n " + ((MainActivity) getActivity()).players.get(i).throwenNumbers.toString().replace("[", "").replace("]", "") + "\n" +
                                    "Průměrná hodnota: " + ((MainActivity) getActivity()).players.get(i).diameterValue;
                        }else message+="\n\nHráč: " + (((MainActivity) getActivity()).players.get(i).name);


                    }

                }

                ((MainActivity)getActivity()).openDrawer(((MainActivity) getActivity()).players);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    //Create new instance of this fragment
    public static PlaygroundFragment newInstance() {
        PlaygroundFragment fragment = new PlaygroundFragment();
        return fragment;
    }

}