package com.tomasvitek.kostky.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import com.tomasvitek.kostky.Cube;
import com.tomasvitek.kostky.MainActivity;
import com.tomasvitek.kostky.Player;
import com.tomasvitek.kostky.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Tomáš Vítek on 05.05.2016 at 19:16.
 */
public class PlayTable extends View {

    //declare variables
    private int x, x1, x2, x3, x4, y, y1, y2, y3, y4, num, time, cubeCount, change, c, sequence = 0, i = 0;
    private float r;
    private boolean goBack = false, isClicked = false;
    private boolean goDownRight = false, goUpLeft = false, goUpRight = false, goDownLeft = false;
    private Random random;
    private View v;
    private Bitmap back;
    private Paint black, blackSmall,blackMini;
    private ArrayList<Player> players;
    private ArrayList<Integer> numbers;
    private List<Cube> cubes;
    private Context context;

    public PlayTable(Context context, final View view) {
        super(context);

        this.context = context;
        players = ((MainActivity) context).names;

        //primary values
        x = 0;
        r = 0;
        y = 0;
        change = 50;
        time = 2000;

        numbers = new ArrayList<>();
        back = BitmapFactory.decodeResource(getResources(), R.drawable.wood_background);

        this.v = view;

        //if view is clicked start animation
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                time = 0;
                isClicked = true;
                invalidate();


                if (i == 20) {

                } else {
                    i = 20;
                }


            }
        });

    }


    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);

        //add time
        time++;


        black = new Paint();
        black.setColor(Color.BLACK);
        black.setStyle(Paint.Style.FILL);
        black.setAlpha(100);
        black.setTextSize(200);

        blackSmall = new Paint();
        blackSmall.setColor(Color.BLACK);
        blackSmall.setStyle(Paint.Style.FILL);
        blackSmall.setAlpha(100);
        blackSmall.setTextSize(100);

        blackMini = new Paint();
        blackMini.setColor(Color.BLACK);
        blackMini.setStyle(Paint.Style.FILL);
        blackMini.setAlpha(100);
        blackMini.setTextSize(60);


        //if time is under 5 sec rotate
        if (time < 300) {

            //Sum must start by zero
            c = 0;

            //Move and disable clicking view
            invalidate();
            v.setClickable(false);

            //Draw cubes
            canvas.drawBitmap(back, 0, 0, null);
            drawCube(canvas);

            //Sum throwen numbers
            for (int a = 0; a < cubeCount; a++) {
                c += cubes.get(a).number;
            }

            //Save players
            ((MainActivity) context).setPlayers(players);

            //Display who play and which number he threw
            canvas.drawText("Hraje " + ((MainActivity) context).players.get(sequence).name, 265, 105, blackSmall);
            if (sequence+1 == players.size()) {
                canvas.drawText("Následuje " + ((MainActivity) context).players.get(0).name, 265, 180, blackMini);
            }else {
                canvas.drawText("Následuje " + ((MainActivity) context).players.get(sequence+1).name, 265, 180, blackMini);
            }
            canvas.drawText(String.valueOf(c), 35, 165, black);

        } else if (time > 1000) {

            //Save players
            ((MainActivity) context).setPlayers(players);

            //Draw background
            back = Bitmap.createScaledBitmap(back, canvas.getWidth(), canvas.getHeight(), false);

            //Draw cubes and display who will play first
            canvas.drawBitmap(back, 0, 0, null);
            drawCube(canvas);
            canvas.drawText("Hraje " + ((MainActivity) context).players.get(sequence).name, 265, 105, blackSmall);
            if (sequence+1 == players.size()) {
                canvas.drawText("Následuje " + ((MainActivity) context).players.get(0).name, 265, 180, blackMini);
            }else {
                canvas.drawText("Následuje " + ((MainActivity) context).players.get(sequence+1).name, 265, 180, blackMini);
            }

        } else {



            //Draw cubes and background
            canvas.drawBitmap(back, 0, 0, null);
            drawCube(canvas);

            //Sum must start by zero
            c = 0;
            for (int a = 0; a < cubeCount; a++) c += cubes.get(a).number;

            //Save players
            ((MainActivity) context).setPlayers(players);

            //Show who plays
            canvas.drawText("Hraje " + ((MainActivity) context).players.get(sequence).name, 265, 105, blackSmall);

            if (sequence+1 == players.size()) {
                canvas.drawText("Následuje " + ((MainActivity) context).players.get(0).name, 265, 180, blackMini);
            }else {
                canvas.drawText("Následuje " + ((MainActivity) context).players.get(sequence+1).name, 265, 180, blackMini);
            }
            canvas.drawText(String.valueOf(c), 35, 165, black);

            if (((MainActivity) context).players.get(sequence).throwenNumbers == null) {
                ((MainActivity) context).players.get(sequence).setThrowenNumbers(new ArrayList<Integer>());
                ((MainActivity) context).players.get(sequence).setDiameterValue(0);
                ((MainActivity) context).players.get(sequence).setAllNumbers(0);
                ((MainActivity) context).players.get(sequence).throwenNumbers.add(c);
                ((MainActivity) context).players.get(sequence).diameterValue = c;
                ((MainActivity) context).players.get(sequence).allNumbers = c;
            } else {
                ((MainActivity) context).players.get(sequence).throwenNumbers.add(c);
                ((MainActivity) context).players.get(sequence).allNumbers += c;
                ((MainActivity) context).players.get(sequence).diameterValue = players.get(sequence).allNumbers / players.get(sequence).throwenNumbers.size();
            }
            //Show next player
            sequence++;
            if (sequence == players.size()) sequence = 0;
            v.setClickable(true);
        }

    }

    public void setCube(int cubeCount) {
        this.cubeCount = cubeCount;

        cubes = new ArrayList<>();


        for (int a = 0; a < cubeCount; a++) {

            Cube cube = new Cube();

            if (a == 0) {
                y += 100;
                x += 0;
                r = 0;
            } else {
                x += 0;
                y += 350;
                r = 0;
            }
            cube.setX(x);
            cube.setY(y);
            cube.setR(r);
            cube.setGoBack(goBack);
            cube.setChange(0);
            cube.setGoDownLeft(goDownLeft);
            cube.setGoUpLeft(goUpLeft);
            cube.setGoDownRight(goDownRight);
            cube.setGoUpRight(goUpRight);

            //Generate random number 1 - 6
            Random random = new Random();
            num = random.nextInt(6) + 1;
            cube.setNumber(num);
            cubes.add(cube);
        }

        invalidate();
    }

    private void firstCube(Canvas canvas, float y1, float y2, float x1, float x2, int i) {
        if (y2 < y1 + 145 && y2 > y1 || y2 + 290 > y1 + 145 && y2 + 290 < y1 + 290) {

            if (y1 - cubes.get(i).randY > 40 && x1 + cubes.get(i).randX < canvas.getWidth() - 290) {
                if ((y1 <= y2 + 290 && y1 + 290 >= y2) && (x1 <= x2 + 290 && x1 + 145 >= x2)) {

                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = true;
                    cubes.get(i).goDownLeft = false;
                    cubes.get(i).goDownRight = false;
                }
            }

            if (x1 - cubes.get(i).randX > 40 && y1 + cubes.get(i).randY < canvas.getHeight() - 290) {
                if ((y1 <= y2 + 290 && y1 + 290 >= y2) && (x1 + 145 <= x2 + 290 && x1 + 290 >= x2)) {

                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = false;
                    cubes.get(i).goDownLeft = true;
                    cubes.get(i).goDownRight = false;
                }
            }

        } else {
            if (x1 - cubes.get(i).randX > 40 && y1 + cubes.get(i).randY < canvas.getHeight() - 290) {
                if ((y1 <= y2 + 290 && y1 + 145 >= y2) && (x1 <= x2 + 290 && x1 + 290 >= x2)) {

                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = false;
                    cubes.get(i).goDownLeft = true;
                    cubes.get(i).goDownRight = false;
                }
            }
            if (y1 - cubes.get(i).randY > 40 && x1 + cubes.get(i).randX < canvas.getWidth() - 290) {
                if ((y1 + 145 < y2 + 290 && y1 + 290 >= y2) && (x1 <= x2 + 290 && x1 + 290 >= x2)) {

                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = true;
                    cubes.get(i).goDownLeft = false;
                    cubes.get(i).goDownRight = false;
                }
            }

        }
    }

    private void secondCube(Canvas canvas, float y1, float y2, float x1, float x2, int i) {
        if (y2 < y1 + 145 && y2 > y1 || y2 + 290 > y1 + 145 && y2 + 290 < y1 + 290) {

            if (y1 - cubes.get(i).randY > 40 && x1 + cubes.get(i).randX < canvas.getWidth() - 290) {
                if ((y2 <= y1 + 290 && y2 + 290 >= y1) && (x2 <= x1 + 290 && x2 + 145 >= x1)) {

                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = true;
                    cubes.get(i).goDownLeft = false;
                    cubes.get(i).goDownRight = false;
                }
            }
            if (x1 - cubes.get(i).randX > 40 && y1 + cubes.get(i).randY < canvas.getHeight() - 290) {
                if ((y2 <= y1 + 290 && y2 + 290 >= y1) && (x2 + 145 <= x1 + 290 && x2 + 290 >= x1)) {

                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = false;
                    cubes.get(i).goDownLeft = true;
                    cubes.get(i).goDownRight = false;
                }
            }


        } else {
            if (x1 - cubes.get(i).randX > 40 && y1 + cubes.get(i).randY < canvas.getHeight() - 290) {
                if ((y2 <= y1 + 290 && y2 + 145 >= y1) && (x2 <= x1 + 290 && x2 + 290 >= x1)) {

                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = false;
                    cubes.get(i).goDownLeft = true;
                    cubes.get(i).goDownRight = false;
                }
            }
            if (y1 - cubes.get(i).randY > 40 && x1 + cubes.get(i).randX < canvas.getWidth() - 290) {
                if ((y2 + 145 < y1 + 290 && y2 + 290 >= y1) && (x2 <= x1 + 290 && x2 + 290 >= x1)) {

                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = true;
                    cubes.get(i).goDownLeft = false;
                    cubes.get(i).goDownRight = false;
                }
            }
        }
    }

    private void drawCube(Canvas canvas) {

        if (isClicked) {

            for (int i = 0; i < cubeCount; i++) {

                //Every 0,3 sec generate new number
                if (cubes.get(i).change >= 15) {

                    //Generate random number 1 - 6
                    Random random = new Random();
                    num = random.nextInt(6) + 1;
                    cubes.get(i).change = 0;
                    cubes.get(i).number = num;

                } else cubes.get(i).change++;


                //create random bouncing animation
                if (cubes.get(i).x >= canvas.getWidth() - 290) {

                    cubes.get(i).goUpLeft = true;
                    cubes.get(i).goUpRight = false;
                    cubes.get(i).goDownLeft = false;
                    cubes.get(i).goDownRight = false;

                    random = new Random();
                    cubes.get(i).randX = random.nextInt(12) + 8;
                    cubes.get(i).randY = random.nextInt(12) + 8;
                    cubes.get(i).randR = random.nextFloat() * (12) + 10;
                }

                if (cubes.get(i).x <= 40) {
                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = false;
                    cubes.get(i).goDownLeft = false;
                    cubes.get(i).goDownRight = true;

                    random = new Random();
                    cubes.get(i).randX = random.nextInt(12) + 8;
                    cubes.get(i).randY = random.nextInt(12) + 8;
                    cubes.get(i).randR = random.nextFloat() * (12) + 10;

                }

                //create random bouncing animation
                if (cubes.get(i).y >= canvas.getHeight() - 290) {

                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = true;
                    cubes.get(i).goDownLeft = false;
                    cubes.get(i).goDownRight = false;

                    random = new Random();
                    cubes.get(i).randX = random.nextInt(12) + 8;
                    cubes.get(i).randY = random.nextInt(12) + 8;
                    cubes.get(i).randR = random.nextFloat() * (12) + 8;
                }

                if (cubes.get(i).y <= 40) {
                    cubes.get(i).goUpLeft = false;
                    cubes.get(i).goUpRight = false;
                    cubes.get(i).goDownLeft = true;
                    cubes.get(i).goDownRight = false;

                    random = new Random();
                    cubes.get(i).randX = random.nextInt(12) + 5;
                    cubes.get(i).randY = random.nextInt(12) + 5;
                    cubes.get(i).randR = random.nextFloat() * (10) + 8;

                }

                if (i == 0) {

                    if (cubeCount == 4) {
                        firstCube(canvas, cubes.get(0).y, cubes.get(3).y, cubes.get(0).x, cubes.get(3).x, i);
                    }
                    if (cubeCount >= 3) {
                        firstCube(canvas, cubes.get(0).y, cubes.get(2).y, cubes.get(0).x, cubes.get(2).x, i);
                    }
                    if (cubeCount >= 2) {
                        firstCube(canvas, cubes.get(0).y, cubes.get(1).y, cubes.get(0).x, cubes.get(1).x, i);
                    }
                }

                if (i == 1) {

                    secondCube(canvas, cubes.get(0).y, cubes.get(1).y, cubes.get(0).x, cubes.get(1).x, i);

                    if (cubeCount == 4) {
                        firstCube(canvas, cubes.get(1).y, cubes.get(3).y, cubes.get(1).x, cubes.get(3).x, i);
                    }

                    if (cubeCount >= 3) {
                        firstCube(canvas, cubes.get(1).y, cubes.get(2).y, cubes.get(1).x, cubes.get(2).x, i);
                    }
                }
                if (i == 2) {

                    secondCube(canvas, cubes.get(1).y, cubes.get(2).y, cubes.get(1).x, cubes.get(2).x, i);
                    secondCube(canvas, cubes.get(0).y, cubes.get(2).y, cubes.get(0).x, cubes.get(2).x, i);

                    if (cubeCount == 4) {
                        firstCube(canvas, cubes.get(2).y, cubes.get(3).y, cubes.get(2).x, cubes.get(3).x, i);
                    }

                }
                if (i == 3) {

                    secondCube(canvas, cubes.get(0).y, cubes.get(3).y, cubes.get(0).x, cubes.get(3).x, i);
                    secondCube(canvas, cubes.get(1).y, cubes.get(3).y, cubes.get(1).x, cubes.get(3).x, i);
                    secondCube(canvas, cubes.get(2).y, cubes.get(3).y, cubes.get(2).x, cubes.get(3).x, i);

                }


                if (cubes.get(i).goDownRight) {
                    if (cubes.get(i).x < canvas.getWidth() - 290) {
                        if (cubes.get(i).x + cubes.get(i).randX >= canvas.getWidth() - 290) {
                            cubes.get(i).x = canvas.getWidth() - 290;
                        } else cubes.get(i).x += cubes.get(i).randX;


                    }

                    if (cubes.get(i).y < canvas.getHeight() - 290) {
                        if (cubes.get(i).y + cubes.get(i).randY >= canvas.getHeight() - 290) {
                            cubes.get(i).y = canvas.getHeight() - 290;
                        } else cubes.get(i).y += cubes.get(i).randY;

                    }
                    cubes.get(i).r -= cubes.get(i).randR;
                }

                if (cubes.get(i).goDownLeft) {
                    if (cubes.get(i).x > 0) {
                        if (cubes.get(i).x - cubes.get(i).randX <= 0) {
                            cubes.get(i).x = 0;
                        } else cubes.get(i).x -= cubes.get(i).randX;

                    }
                    if (cubes.get(i).y < canvas.getHeight() - 290) {
                        if (cubes.get(i).y + cubes.get(i).randY >= canvas.getHeight() - 290) {
                            cubes.get(i).y = canvas.getHeight() - 290;
                        } else cubes.get(i).y += cubes.get(i).randY;

                    }
                    cubes.get(i).r += cubes.get(i).randR;
                }

                if (cubes.get(i).goUpRight) {
                    if (cubes.get(i).x < canvas.getWidth() - 290) {
                        if (cubes.get(i).x + cubes.get(i).randX >= canvas.getWidth() - 290) {
                            cubes.get(i).x = canvas.getWidth() - 290;
                        } else cubes.get(i).x += cubes.get(i).randX;
                    }
                    if (cubes.get(i).y > 0) {
                        if (cubes.get(i).y - cubes.get(i).randY <= 0) {
                            cubes.get(i).y = 0;
                        } else cubes.get(i).y -= cubes.get(i).randY;

                    }
                    cubes.get(i).r += cubes.get(i).randR;
                }
                if (cubes.get(i).goUpLeft) {
                    if (cubes.get(i).x > 0) {
                        if (cubes.get(i).x - cubes.get(i).randX <= 0) {
                            cubes.get(i).x = 0;
                        } else cubes.get(i).x -= cubes.get(i).randX;

                    }

                    if (cubes.get(i).y > 0) {
                        if (cubes.get(i).y - cubes.get(i).randY <= 0) {
                            cubes.get(i).y = 0;
                        } else cubes.get(i).y -= cubes.get(i).randY;

                    }
                    cubes.get(i).r -= cubes.get(i).randR;
                }


                //draw setted quantity cubes
                cubes.get(i).onDrawCube(canvas);
            }
        } else {

            //only draw cubes
            for (int i = 0; i < cubeCount; i++) cubes.get(i).onDrawCube(canvas);
        }
    }
}